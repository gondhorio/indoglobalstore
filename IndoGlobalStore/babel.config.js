/*
module.exports = function(api) {
  api.cache(false);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      'react-native-reanimated/plugin'
    ],
  };
};
*/
/*
const pak = require('./package.json');
module.exports = {
  presets: ['babel-preset-expo','module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.tsx', '.ts', '.js', '.json'],
      },
      
    ],
    'react-native-reanimated/plugin' // PUT IT HERE
  ]
};
*/
module.exports = function (api) {
  api.cache(true);

  return {
    presets: ["babel-preset-expo"],
    env: {
      development: {
        plugins: ["react-native-reanimated/plugin"],
      },
      production: {
        //plugins: ["react-native-reanimated/plugin", "react-native-paper/babel"],
        plugins: ["react-native-reanimated/plugin"],
      },
    },
    plugins: ["react-native-reanimated/plugin"]
  };
};

