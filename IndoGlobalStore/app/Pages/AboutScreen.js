import React from 'react'
import { StyleSheet, Text, View, Image, FlatList, ScrollView,TouchableOpacity, TextInput } from 'react-native'
import { Data } from '../data'
import {MaterialCommunityIcons} from "react-native-vector-icons";

export default function AboutScreen({ route, navigation }) {

   // const { itemId } = route.params;
//	console.log("itemId",itemId);

    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };
    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        
    }

    return (
        <View style={styles.container}>
            <View style={{width:'100%'}}>
                <Image
                    style={{ resizeMode:'stretch', height:80, width:'100%', }}
                    source={require('../assets/header.png')}
                />
                <Text style={{position:'absolute', bottom:2, right:5, fontWeight:'bold', fontSize:18,}}>Indo Global Store</Text>
            </View>
                           

            <View style={{ backgroundColor:'blue', height:30, width:'100%', color:'white',}}>
                <TouchableOpacity style={{flexDirection:'row'}}>                    
                    <MaterialCommunityIcons style={{padding:5,}}
                            name="account-tie"
                            color="white"
                            size={20}
                        />
                    <Text style={{padding:5, paddingLeft:0, paddingTop:7, color:'white',fontSize:12, fontWeight:'bold',}}>My Profiles</Text>                   
                </TouchableOpacity>                    
            </View>

            <View style={{width:'100%', padding:10, flex:1, height:'100%',}}>

                <View style={{padding:5, flexDirection:'row', height:120,}}>
                    <Image
                        style={{ resizeMode:'contain', height:100, width:100, borderRadius:100, }}
                        source={require('../assets/pp.png')}
                    />
                    <View style={{paddingLeft:10,justifyContent:'center', height:'100%'}}>
                        <Text style={{fontWeight:'bold', fontSize:16,}}>Khomsun</Text>
                        <Text style={{fontSize:12, color:'blue'}}>React Native Developer</Text>
                    </View>
                </View>

                <View style={{padding:5, borderStyle:'solid', borderTopWidth:0.3, borderColor:'blue'}}>                   
                </View>

                <View style={{flexDirection:'row', marginTop:5}}>
                    <MaterialCommunityIcons
                            name="linkedin"
                            color="black"
                            size={24}
                        />
                        <Text style={{marginLeft:5}}>@khomsun</Text>
                </View>
                <View style={{flexDirection:'row', marginTop:5}}>
                    <MaterialCommunityIcons
                            name="facebook"
                            color="black"
                            size={24}
                        />
                        <Text style={{marginLeft:5}}>Homzun Ozora</Text>
                </View>
                <View style={{flexDirection:'row', marginTop:5}}>
                    <MaterialCommunityIcons
                            name="email-outline"
                            color="black"
                            size={24}
                        />
                        <Text style={{marginLeft:5}}>gondhorio@gmail.com</Text>
                </View>

            </View>
           
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems:'center',
        backgroundColor:'white',
        
    },
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    rowItem: {
        width:'100%', 
        margin:0, 
        padding:6, 
        borderStyle:"solid", 
        borderColor:"blue", 
        borderWidth:0.2,
        flexDirection:'row',          
    },
})

