
import React from "react";
import {StyleSheet, Text, View} from "react-native";
import SplashScreen2 from "./app/index";

import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import { useState } from "react";
import { getApps, initializeApp } from "firebase/app";

import { Provider } from "react-redux";
import { store } from "./app/Redux/store";

const firebaseConfig = {
    apiKey: "AIzaSyDMOwZpjNQ9uFvhchUqsCNO7COn6KBRl-0",
    authDomain: "sanbercode-7af8e.firebaseapp.com",
    projectId: "sanbercode-7af8e",
    storageBucket: "sanbercode-7af8e.appspot.com",
    messagingSenderId: "160026504127",
    appId: "1:160026504127:web:dbd60c0f93c06c7c090f0a"
  };
  

// Initialize Firebase
if (!getApps().length) {
	const app = initializeApp(firebaseConfig);
}


export default function App(){
    return (
        <Provider store={store}>
			<StatusBar style="light" translucent={false} />
			
			<SplashScreen2 />

		</Provider>
    );
}

const styles = StyleSheet.create({
    container:{
        flex : 1,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
})


